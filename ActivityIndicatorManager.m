//
//  ActivityIndicatorManager.m
//  TVBZone
//
//  Created by Ted Cheng on 4/1/13.
//  Copyright (c) 2013 Ted Cheng. All rights reserved.
//

#import "ActivityIndicatorManager.h"

static NSMutableArray *_activities;

@implementation ActivityIndicatorManager

+ (NSMutableArray *) currentActivites
{
    if (!_activities) {
        _activities = [[NSMutableArray alloc] init];
    }
    return _activities;
}

+ (void)showIndicatorForActivity:(NSString *)activity
{
#ifdef ActivityIndicatorManagerShouldLog
    NSLog(@"*** ActivityIndicatorManager - SHOW - %@", activity);
#endif
    
    if (![self isActivityExist:activity]) {
        [[self currentActivites] addObject:activity];
    }
    
    if ([[self currentActivites] count] > 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }

#ifdef ActivityIndicatorManagerShouldLogDetails
    NSLog(@"*** ActivityIndicatorManager - CURRENT - %@", [self currentActivites]);
#endif
    
}

+ (void)dismissIndicatorForActivity:(NSString *)activity
{
#ifdef ActivityIndicatorManagerShouldLog
    NSLog(@"*** ActivityIndicatorManager - DISMISS - %@", activity);
#endif
    
    if ([self isActivityExist:activity]) {
        [[self currentActivites] removeObject:activity];      
    }

    if ([[self currentActivites] count] == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
    
#ifdef ActivityIndicatorManagerShouldLogDetails
    NSLog(@"*** ActivityIndicatorManager - CURRENT - %@", [self currentActivites]);
#endif
    
}

+ (BOOL)isActivityExist:(NSString *) activity
{
    return [[self currentActivites] containsObject:activity];
}

@end
