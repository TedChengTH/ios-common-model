//
//  DataDownloader.m
//  TVBZone
//
//  Created by Ted Cheng on 13/12/12.
//  Copyright (c) 2012 Ted Cheng. All rights reserved.
//

#import "DataDownloader.h"
#import "ActivityIndicatorManager.h"

#define kRequestTimeOut 60

static NSMutableArray *_currentDownloaders;

static NSMutableArray *_backgroundDownloaders;

@interface DataDownloader()

@property (nonatomic, strong) NSURLConnection *connection;

@property (nonatomic, strong) NSMutableData *data;

@property (nonatomic, strong) NSURL *url;

@property (nonatomic, strong) NSString *tag;

@property (copy, nonatomic) void (^completionHandler) (NSData*);

@end

@implementation DataDownloader

+ (NSMutableArray *) currentDownloaders
{
    if (!_currentDownloaders) {
        _currentDownloaders = [[NSMutableArray alloc] init];
    }

    return _currentDownloaders;
}

+ (NSMutableArray *) backgroundDownloaders
{
    if (!_backgroundDownloaders) {
        _backgroundDownloaders = [[NSMutableArray alloc] init];
    }
    return _backgroundDownloaders;
}

+ (DataDownloader *) downloaderWithTag:(NSString *) tag
{
    for (DataDownloader *dataDownloader in [self currentDownloaders]) {
        if (dataDownloader) {
            if ([dataDownloader.tag isEqualToString: tag]) return dataDownloader;
        }
    }
    return nil;
}

+ (void)downloadURL:(NSURL *) url tag:(NSString *) tag completion:(void (^)(NSData *data))completion
{
    if ([self downloaderWithTag:tag]) return;
    
    DataDownloader *downloader = [[DataDownloader alloc] initWithURL:url tag:tag completion:completion];
    [[DataDownloader currentDownloaders] addObject:downloader];
    
#ifdef DataDownloaderShouldLogDetails
    NSLog(@"*** DataDownloader - Current - %@", [DataDownloader currentDownloaders]);
#endif
    
    [downloader startDownload];
}

+ (void)cancelDownloaderWithTag:(NSString *)tag
{
    DataDownloader *downloader = [DataDownloader downloaderWithTag:tag];
    if (downloader) {
        [downloader cancelDownload];
        [[self currentDownloaders] removeObject:downloader];
        
#ifdef DataDownloaderShouldLogDetails
        NSLog(@"*** DataDownloader - Current - %@", [DataDownloader currentDownloaders]);
#endif
    }
}

+ (void)cancelAllDownloaders
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[self currentDownloaders] makeObjectsPerformSelector:@selector(cancelDownload)];        
    });
}

#pragma mark - instance method

- (id)initWithURL:(NSURL *) url tag:(NSString *) tag completion:(void (^)(NSData *data))completion
{
    self = [super init];
    if (self) {
        self.url = url;
        self.tag = tag;
        self.completionHandler = completion;
    }return self;
}

- (void)startDownload
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.url];
    [request setTimeoutInterval:kRequestTimeOut];
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    [ActivityIndicatorManager showIndicatorForActivity:self.tag];
#ifdef DataDownloaderShouldLog
    NSLog(@"*** DataDownloader - Download With Tag %@ - START", self.tag);
    NSLog(@"*** DataDownloader - Download With Tag %@ - URL = %@", self.tag, self.url);    
#endif

}

- (void)cancelDownload
{
    [self.connection cancel];
    [[DataDownloader currentDownloaders] removeObject:self];
    [ActivityIndicatorManager dismissIndicatorForActivity:self.tag];
    
#ifdef DataDownloaderShouldLog
    NSLog(@"*** DataDownloader - Download With Tag %@ - CANCEL", self.tag);
#endif
    
#ifdef DataDownloaderShouldLogDetails
    NSLog(@"*** DataDownloader - Current - %@", [DataDownloader currentDownloaders]);
#endif
}

#pragma NSData delegate 

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
#ifdef DataDownloaderShouldLog
    NSLog(@"*** DataDownloader - Download With Tag %@ - ERROR - %@", self.tag ,error);
#endif
    [self cancelDownload];
    self.completionHandler(nil);
}

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData
{
    if (self.data == nil)
        self.data = [[NSMutableData alloc] init];
    [self.data appendData:incrementalData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)theConnection
{
    [[DataDownloader currentDownloaders] removeObject:self];
    
    [ActivityIndicatorManager dismissIndicatorForActivity:self.tag];
#ifdef DataDownloaderShouldLog
    NSLog(@"*** DataDownloader - Download With Tag %@ - DONE", self.tag);
#endif
    
#ifdef DataDownloaderShouldLogDetails
    NSLog(@"*** DataDownloader - Current - %@", [DataDownloader currentDownloaders]);
#endif
    self.completionHandler(self.data);
    
}


@end
