//
//  DataDownloader.h
//  TVBZone
//
//  Created by Ted Cheng on 13/12/12.
//  Copyright (c) 2012 Ted Cheng. All rights reserved.
//

#define DataDownloaderShouldLog 
#define DataDownloaderShouldLogDetails

#import <Foundation/Foundation.h>

@interface DataDownloader : NSObject <NSURLConnectionDataDelegate>

+ (void)downloadURL:(NSURL *) url tag:(NSString *) tag completion:(void (^)(NSData *data))completion;

+ (void)cancelDownloaderWithTag:(NSString *) tag;

+ (void)cancelAllDownloaders;

@end
